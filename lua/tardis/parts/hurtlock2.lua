local PART={}
PART.ID = "hurtlock2"
PART.Name = "War Doctor's TARDIS Safe Lock 2"
PART.Model = "models/doctormemes/hurt/safe lock 2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.4
PART.Sound = "doctormemes/hurt/skrrrra.wav"
PART.Control = "vortex_flight"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

