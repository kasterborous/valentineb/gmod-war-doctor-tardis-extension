local PART={}
PART.ID = "hurtcs1"
PART.Name = "War Doctor's TARDIS Control Switch 1"
PART.Model = "models/doctormemes/hurt/cs1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/toggle.wav"
PART.Control = "isomorphic"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

