local PART={}
PART.ID = "hurtcs3"
PART.Name = "War Doctor's TARDIS Control Switch 3"
PART.Model = "models/doctormemes/hurt/cs3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/toggle.wav"
PART.Control = "toggle_screens"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

