local PART={}
PART.ID = "hurtweirdlever"
PART.Name = "War Doctor's TARDIS Weird Lever"
PART.Model = "models/doctormemes/hurt/weird lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/weird lever.wav"
PART.Control = "physlock"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)
