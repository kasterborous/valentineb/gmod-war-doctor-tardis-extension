local PART={}
PART.ID = "hurtresistor"
PART.Name = "War Doctor's TARDIS Resistor"
PART.Model = "models/doctormemes/hurt/resistor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.56
PART.Sound = "doctormemes/hurt/resistor.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

