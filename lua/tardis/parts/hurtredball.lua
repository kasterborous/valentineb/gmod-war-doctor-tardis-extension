local PART={}
PART.ID = "hurtredball"
PART.Name = "War Doctor's TARDIS Red Ball"
PART.Model = "models/doctormemes/hurt/red ball.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.ShouldTakeDamage = true
PART.Sound = "doctormemes/hurt/ball.wav"
PART.Control = "cloak"

TARDIS:AddPart(PART,e)

