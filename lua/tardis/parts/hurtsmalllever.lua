local PART={}
PART.ID = "hurtsmalllever"
PART.Name = "War Doctor's TARDIS Small Lever"
PART.Model = "models/doctormemes/hurt/small lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctormemes/hurt/Lever 3.wav"
PART.Control = "handbrake"
PART.ShouldTakeDamage = true


TARDIS:AddPart(PART,e)

