local PART={}
PART.ID = "hurtscreen"
PART.Name = "War Doctor's TARDIS Screen"
PART.Model = "models/doctormemes/hurt/screen.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/hurt/screen.wav"
PART.Control = "coordinates"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)