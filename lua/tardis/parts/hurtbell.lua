local PART={}
PART.ID = "hurtbell"
PART.Name = "War Doctor's TARDIS Bell"
PART.Model = "models/doctormemes/hurt/bell.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/hurt/bell.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

