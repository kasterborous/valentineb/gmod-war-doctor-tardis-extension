local PART={}
PART.ID = "hurtconsole"
PART.Name = "War Doctor's TARDIS Console"
PART.Model = "models/doctormemes/hurt/console.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.ShouldTakeDamage = true
PART.BypassIsomorphic = true
PART.Control = "thirdperson_careful"

TARDIS:AddPart(PART,e)