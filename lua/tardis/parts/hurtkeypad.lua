local PART={}
PART.ID = "hurtkeypad"
PART.Name = "War Doctor's TARDIS Keypad"
PART.Model = "models/doctormemes/hurt/keypad.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "doctormemes/hurt/keypad.wav"
PART.Control = "destination"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)