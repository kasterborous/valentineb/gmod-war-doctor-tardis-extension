local PART={}
PART.ID = "hurtblueball"
PART.Name = "War Doctor's TARDIS Blue Ball"
PART.Model = "models/doctormemes/hurt/blue ball.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "doctormemes/hurt/ball.wav"
PART.Control = "repair"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

