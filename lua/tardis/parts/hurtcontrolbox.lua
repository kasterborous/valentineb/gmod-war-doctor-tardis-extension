local PART={}
PART.ID = "hurtcontrolbox"
PART.Name = "War Doctor's TARDIS Control Box"
PART.Model = "models/doctormemes/hurt/control box.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Control = "hads"
PART.SoundOn = "doctormemes/hurt/cb1.wav"
PART.SoundOff = "doctormemes/hurt/cb2.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

