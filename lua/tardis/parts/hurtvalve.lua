local PART={}
PART.ID = "hurtvalve"
PART.Name = "War Doctor's TARDIS Valve"
PART.Model = "models/doctormemes/hurt/valve.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Control = "doorlock"
PART.Sound = "doctormemes/hurt/valve.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

