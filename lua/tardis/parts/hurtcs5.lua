local PART={}
PART.ID = "hurtcs5"
PART.Name = "War Doctor's TARDIS Control Switch 5"
PART.Model = "models/doctormemes/hurt/cs5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/toggle.wav"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

