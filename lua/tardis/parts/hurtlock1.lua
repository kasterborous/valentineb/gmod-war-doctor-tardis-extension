local PART={}
PART.ID = "hurtlock1"
PART.Name = "War Doctor's TARDIS Safe Lock 1"
PART.Model = "models/doctormemes/hurt/safe lock.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.4
PART.Sound = "doctormemes/hurt/skrrrra.wav"
PART.Control ="fastreturn"
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART,e)

