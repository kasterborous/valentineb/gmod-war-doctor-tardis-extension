local PART={}
PART.ID = "hurtcs2"
PART.Name = "War Doctor's TARDIS Control Switch 2"
PART.Model = "models/doctormemes/hurt/cs2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/toggle.wav"
PART.ShouldTakeDamage = true
PART.Control = "redecorate"

TARDIS:AddPart(PART,e)

