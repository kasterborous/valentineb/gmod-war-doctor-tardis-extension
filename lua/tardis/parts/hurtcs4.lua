local PART={}
PART.ID = "hurtcs4"
PART.Name = "War Doctor's TARDIS Control Switch 4"
PART.Model = "models/doctormemes/hurt/cs4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "doctormemes/hurt/toggle.wav"
PART.ShouldTakeDamage = true


TARDIS:AddPart(PART,e)

