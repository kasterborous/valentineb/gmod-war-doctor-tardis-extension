local style={
	style_id="hurt_tips",
	style_name="War Doctor",
	font = "Trebuchet24",
	colors = {
		normal = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 130, 5, 5, 250 ),
			frame = Color( 255, 245, 180, 10 ),
		},
		highlighted = {
			text = Color( 255, 255, 255, 255 ),
			background = Color( 5, 130, 5, 250 ),
			frame = Color( 255, 245, 180, 10 ),
		}
	}
}
TARDIS:AddTipStyle(style)