-- War Doctor's TARDIS

local T={}
T.Base="base"
T.Name="War Doctor's TARDIS"
T.ID="hurt"
T.Interior={
	Model="models/doctormemes/hurt/interior.mdl",
	Portal={
	        pos=Vector(0,-290.64,33),
	        ang=Angle(0,90,0),
		width=180,
		height=160
	},
	Screens={
		{
			pos=Vector(-25.4834,-5.9958,67.6157),
			ang=Angle(0,-60,90),
			width=243,
			height=165,
		  	visgui_rows=2
		}
	},
	ScreensEnabled=false,
	Fallback={
		pos=Vector(0,-260,-10),
		ang=Angle(0,90,0),
	},
	ExitDistance=420,

	Sequences="hurt_sequences",

	Parts={
		hurtconsole=true,
		hurtglass=true,
		hurtscreen=true,
		hurtstruts=true,
		hurtmetalcover=true,
		hurtthrottle=true,
		hurttrash=true,
		hurtbiglever=true,
		hurtbelttrash=true,
		hurttrash2=true,
		hurtblacklever=true,
		hurtphone=true,
		hurtresistor=true,
		hurtfourbuttons=true,
		hurtsmalllever=true,
		hurtredball=true,
		hurtblueball=true,
		hurtbell=true,
		hurtvoicebutton=true,
		hurtvalve=true,
		hurtswitch=true,
		hurtweirdlever=true,
		hurtcs1=true,
		hurtcs2=true,
		hurtcs3=true,
		hurtcs4=true,
		hurtcs5=true,
		hurtcontrolbox=true,
		hurtlock1=true,
		hurtlock2=true,
		hurtconsolelamps=true,
		hurtscreenanimation=true,
		hurtstasiscube=true,
		hurtroom=true,
		hurtpainting=true,
		hurtkeypad=true,



                door={
                        model="models/doctorwho1200/hurt/doors.mdl",posoffset=Vector(4.01,0,-47.971),angoffset=Angle(0,-180,0)
             },
	},
	
	IdleSound={
		{
			path="doctormemes/hurt/interior.wav",
			volume=1
		}
	},
	Light = { color = Color(255,255,255), pos = Vector(0,100,120), brightness = 1, warncolor = Color(255, 0, 0),},
	Lights = {
		{color = Color(255, 255, 255),		pos = Vector(0,-100,120),	brightness = 1,		warncolor = Color(255, 0, 0),},
//		{color = Color(0, 0, 0),		pos = Vector(0,0,0),	brightness = 0,},
//		{color = Color(0, 0, 0),		pos = Vector(0,0,0),	brightness = 0,}, -- these two are blanks. Need to be here because [REDACTED].
	},
	LightOverride={
		basebrightness=0.3,
		nopowerbrightness=0.1
	},
	TipSettings={
			style="hurt_tips",
            		view_range_max=70,
            		view_range_min=65,
	},
	PartTips={
        hurtthrottle	=	{pos=Vector(15.9281 , 29.3276 , 48.8598 ), text="Demat Switch"},
        hurtsmalllever	=	{pos=Vector(25.664 , 32.6448 , 40.3016 ), text="Handbreak"},
        hurtscreen		=	{pos=Vector(-30.1755 , -9.71605 , 55.8349 ), text="Coordinates"},
        hurtblueball	=	{pos=Vector(-21.4891 , 8.55139 , 48.6441 ), text="Repair"},
		hurtbiglever	=   {pos=Vector(26.0681 , 15.0504 , 47.3064 ), text="Flight"}, 
        hurtlock2		=   {pos=Vector(12.5883 , 21.7998 , 50.2743 ), text="Vortex Flight"},
        hurtlock1		=   {pos=Vector(-17.0997 , -34.2553 , 42.0797 ), text="Fast Return"},
        hurtweirdlever		=   {pos=Vector(-4.67012 , -32.7927, 43.2155 ), text="Physical Lock"},
        hurtcs1		=   {pos=Vector(-23.026, -20.766, 44.102 ), text="Security", right = true},
		hurtkeypad		=   {pos=Vector(2.694, 20.132, 57.496 ), text="Manual Destination Selection"},
		hurtcs2		=   {pos=Vector(-25.791, -19.102, 43.171 ), text="Redecoration", right = true, down = true},
	},
	CustomTips={
		{pos=Vector(0.297417 , -21.957 , 49.3534 ),     text="H.A.D.S."},
		{pos=Vector(-6.71457 , 32.3884 , 42.8015 ),     text="Cloaking Device"},
		{pos=Vector(31.2366 , 8.72762 , 46.2531 ),     text="Float"},
		{pos=Vector(-6.14295 , 24.152 , 46.8355 ),     text="Door Switch"},
		{pos=Vector(7.74799 , -32.1924 , 43.236 ),     text="Door Lock"},
		{pos=Vector(-14.3909 , 22.9486 , 51.0305 ),     text="Power"},
		{pos=Vector(-27.166, -16.609, 43.419 ),     text="Visual UI"},
	},
}
T.Exterior={
	Model="models/doctorwho1200/hurt/exterior.mdl",
	Mass=5000,
	ScannerOffset=Vector(30,0,50),	
	Portal={
		pos=Vector(30,0,48),
		ang=Angle(0,0,0),
		width=50,
		height=90
	},
	Fallback={
		pos=Vector(50,0,7),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,119),
		color=Color(255,240,200)
	},
	Sounds={
		Lock="doctormemes/hurt/lock.wav",
		Door={
			enabled=true,
			open="doctormemes/hurt/doorext_open.wav",
			close="doctormemes/hurt/doorext_close.wav"
		}
	},
	Parts={
                door={
                        model="models/doctorwho1200/hurt/doors_ext.mdl",posoffset=Vector(-4.01,0,-47.971),angoffset=Angle(0,0,0)
                		
		},
		vortex={
			model="models/doctormemes/hurt/vortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,90,0),
			scale=10			
		}
	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
}

TARDIS:AddInterior(T)